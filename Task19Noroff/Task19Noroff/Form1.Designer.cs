﻿namespace Task19Noroff {
    partial class Task19Form {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.CreateBtn = new System.Windows.Forms.Button();
            this.ReadBtn = new System.Windows.Forms.Button();
            this.DeleteBtn = new System.Windows.Forms.Button();
            this.UpdateBtn = new System.Windows.Forms.Button();
            this.tableSupervisors = new System.Windows.Forms.DataGridView();
            this.txtNameInput = new System.Windows.Forms.TextBox();
            this.txtIdInput = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.tableSupervisors)).BeginInit();
            this.SuspendLayout();
            // 
            // CreateBtn
            // 
            this.CreateBtn.Location = new System.Drawing.Point(578, 315);
            this.CreateBtn.Name = "CreateBtn";
            this.CreateBtn.Size = new System.Drawing.Size(102, 36);
            this.CreateBtn.TabIndex = 0;
            this.CreateBtn.Text = "Create";
            this.CreateBtn.UseVisualStyleBackColor = true;
            // 
            // ReadBtn
            // 
            this.ReadBtn.Location = new System.Drawing.Point(686, 315);
            this.ReadBtn.Name = "ReadBtn";
            this.ReadBtn.Size = new System.Drawing.Size(102, 36);
            this.ReadBtn.TabIndex = 1;
            this.ReadBtn.Text = "Read";
            this.ReadBtn.UseVisualStyleBackColor = true;
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.Location = new System.Drawing.Point(117, 326);
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.Size = new System.Drawing.Size(98, 35);
            this.DeleteBtn.TabIndex = 2;
            this.DeleteBtn.Text = "Delete";
            this.DeleteBtn.UseVisualStyleBackColor = true;
            // 
            // UpdateBtn
            // 
            this.UpdateBtn.Location = new System.Drawing.Point(117, 274);
            this.UpdateBtn.Name = "UpdateBtn";
            this.UpdateBtn.Size = new System.Drawing.Size(98, 35);
            this.UpdateBtn.TabIndex = 3;
            this.UpdateBtn.Text = "Update";
            this.UpdateBtn.UseVisualStyleBackColor = true;
            // 
            // tableSupervisors
            // 
            this.tableSupervisors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tableSupervisors.Location = new System.Drawing.Point(241, 52);
            this.tableSupervisors.Name = "tableSupervisors";
            this.tableSupervisors.RowHeadersWidth = 51;
            this.tableSupervisors.RowTemplate.Height = 24;
            this.tableSupervisors.Size = new System.Drawing.Size(547, 257);
            this.tableSupervisors.TabIndex = 4;
            // 
            // txtNameInput
            // 
            this.txtNameInput.Location = new System.Drawing.Point(31, 52);
            this.txtNameInput.Name = "txtNameInput";
            this.txtNameInput.Size = new System.Drawing.Size(184, 22);
            this.txtNameInput.TabIndex = 5;
            // 
            // txtIdInput
            // 
            this.txtIdInput.Location = new System.Drawing.Point(31, 91);
            this.txtIdInput.Name = "txtIdInput";
            this.txtIdInput.Size = new System.Drawing.Size(184, 22);
            this.txtIdInput.TabIndex = 6;
            // 
            // Task19Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.txtIdInput);
            this.Controls.Add(this.txtNameInput);
            this.Controls.Add(this.tableSupervisors);
            this.Controls.Add(this.UpdateBtn);
            this.Controls.Add(this.DeleteBtn);
            this.Controls.Add(this.ReadBtn);
            this.Controls.Add(this.CreateBtn);
            this.Name = "Task19Form";
            this.Text = "Task 19";
            ((System.ComponentModel.ISupportInitialize)(this.tableSupervisors)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CreateBtn;
        private System.Windows.Forms.Button ReadBtn;
        private System.Windows.Forms.Button DeleteBtn;
        private System.Windows.Forms.Button UpdateBtn;
        private System.Windows.Forms.DataGridView tableSupervisors;
        private System.Windows.Forms.TextBox txtNameInput;
        private System.Windows.Forms.TextBox txtIdInput;
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Task19Noroff.Model {
    class Task19NoroffDBContext : DbContext {

        public DbSet<Supervisor> Supervisors { get; set; }

    }
}
